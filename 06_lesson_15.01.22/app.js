/*
Thank you for registering with the open platform.
A new key has been created for you: 3534a47c-f615-488a-ac2b-1cbb3a5481ee
You can try this key by accessing https://content.guardianapis.com/search?api-key=3534a47c-f615-488a-ac2b-1cbb3a5481ee in your browser.
*/

// https://open-platform.theguardian.com/documentation/
// Example: https://content.guardianapis.com/search?page=2&q=debate&api-key=test

// function myProm(resolve, reject) {
//     // resolve({ name: "Vova", age: 25 });

//     if (Math.random() > 0.5) {
//         let result = [
//             { name: "Vova", age: 25 },
//             { name: "Sara", age: 20 },
//             { name: "Bob", age: 27 },
//         ];
//         console.log(result);

//         return resolve(result);
//     }

//     let err = "Database error...";
//     console.log(err);
//     reject(err);
// }

function myProm(resolve, reject) {
    // if (Math.random() > 0.5) {
    //     let result = [
    //         { name: "Vova", age: 25 },
    //         { name: "Sara", age: 20 },
    //         { name: "Bob", age: 27 },
    //     ];
    //     return resolve(result);
    // }

    resolve(10);

    // reject("Promise error");

    // let err = "Database error...";
    // reject(err);
}

let promiseVar = new Promise(myProm);

// promiseVar
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((err) => {
//         console.log(err);
//     });

promiseVar
    .then((data) => {
        console.log(data);
        return data * 2;
    })
    .then((qwe) => {
        console.log(qwe);
        return qwe * 3;
    })
    .then((pizza) => {
        console.log(pizza);
    })

    .catch((err) => {
        console.log(err);
    });
