class News {
    constructor(url) {
        this.url = url;
        this.list = document.querySelector(".list");
        this.prevBtn = document.querySelector("#prev");
        this.nextBtn = document.querySelector("#next");
        this.input = document.querySelector('input[name="pageNumber"]');
        this.span = document.querySelector("span");
        this.pageCounter = 1;
        this.searchForm = document.querySelector(".js-form");
        this.searchValue = "";
        this.totalPages = null;
    }

    fetchNews = () => {
        const query = `&q=${this.searchValue}`;
        let url = this.url + this.pageCounter + query;
        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                console.log(data.response);
                if (data.response.status === "ok") {
                    this.totalPages = Number(data.response.pages);
                    this.renderNews(data.response.results);
                    this.renderPagination(data.response);
                }
            });
    };

    renderPagination = (pageResponse) => {
        this.input.value = pageResponse.currentPage;
        this.span.textContent = `from: ${pageResponse.pages}`;
    };

    renderNews = (newsArray) => {
        this.list.innerHTML = "";
        const markup = newsArray.map(
            ({ webPublicationDate, webTitle, webUrl }) => {
                const li = document.createElement("li");
                const a = document.createElement("a");
                const p = document.createElement("p");

                a.href = webUrl;
                a.textContent = webTitle;
                a.target = "_blank";
                p.textContent = `Publication date: ${webPublicationDate}`;

                a.append(p);
                li.append(a);
                return li;
            }
        );
        this.list.append(...markup);
    };

    onInputChange = (event) => {
        this.pageCounter = event.target.value;
        this.fetchNews();
    };

    onNextBtnClick = (event) => {
        this.pageCounter += 1;
        this.fetchNews();
    };

    onPrevBtnClick = () => {
        this.pageCounter -= 1;
        this.fetchNews();
    };

    onFormSubmit = (event) => {
        event.preventDefault();
        const searchValue = event.target.elements.search.value;
        this.searchValue = searchValue;
        this.fetchNews();
    };

    addListeners = () => {
        window.addEventListener("load", this.fetchNews);
        this.input.addEventListener("input", this.onInputChange);
        this.nextBtn.addEventListener("click", this.onNextBtnClick);
        this.prevBtn.addEventListener("click", this.onPrevBtnClick);
        this.searchForm.addEventListener("submit", this.onFormSubmit);
    };

    init = () => {
        this.addListeners();
    };
}
const API_KEY = "3dd8afe6-1413-45ac-8920-c9e61909f98f";
const BASE_URL = `https://content.guardianapis.com/search?api-key=${API_KEY}&page=`;
const news = new News(BASE_URL);
news.init();
