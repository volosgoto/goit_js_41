class Menu {
    constructor(diseases) {
        this.diseases = diseases;
        this.rootRef = document.querySelector("#root");
        this.menuBtn = document.querySelector("#menu");
        this.ul = null;
    }

    vova = "vova";

    sara = () => {
        // console.log("sara");
    };

    onBtnMenuClick = () => {
        console.log(this);

        if (!this.ul) {
            this.createUl();
        }

        if (this.ul.classList.contains("hide")) {
            this.ul.classList.add("show");
            this.ul.classList.remove("hide");
            this.menuBtn.textContent = "hide menu";
        } else {
            this.ul.classList.add("hide");
            this.ul.classList.remove("show");
            this.menuBtn.textContent = "show menu";
        }
    };

    // onBtnMenuClick() {
    //     if (!this.ul) {
    //         this.createUl();
    //     }

    //     if (this.ul.classList.contains("hide")) {
    //         this.ul.classList.add("show");
    //         this.ul.classList.remove("hide");
    //         this.menuBtn.textContent = "hide menu";
    //     } else {
    //         this.ul.classList.add("hide");
    //         this.ul.classList.remove("show");
    //         this.menuBtn.textContent = "show menu";
    //     }
    // }

    createMenu() {
        return this.diseases.map((menuItem) => {
            return this.createLi(menuItem);
        });
    }

    createUl() {
        this.ul = document.createElement("ul");
        this.ul.classList.add("hide");
        // console.log(this.ul);

        const li = this.createMenu();
        // console.log(li);
        this.ul.append(...li);
        // console.log(this.ul);

        this.rootRef.append(this.ul);

        console.log(this.rootRef);
    }

    createLi(menuItem) {
        const { name, href } = menuItem;
        let li = document.createElement("li");
        let a = document.createElement("a");

        a.setAttribute("href", href);
        a.setAttribute("target", "_blank");
        a.textContent = name;

        a.classList.add("item__link");
        li.classList.add("item__list");

        li.append(a);

        return li;
    }

    addListeners() {
        // console.log(this);
        // this.menuBtn.addEventListener("click", this.onBtnMenuClick.bind(this));
        this.menuBtn.addEventListener("click", this.onBtnMenuClick);
    }

    init() {
        this.menuBtn.textContent = "show menu";
        this.addListeners();
    }
}

const diseases = [
    {
        name: "Plague",
        href: "https://en.wikipedia.org/wiki/Plague_(disease)",
    },
    {
        name: "Anthrax",
        href: "https://en.wikipedia.org/wiki/Anthrax",
    },
    {
        name: "Ebola",
        href: "https://en.wikipedia.org/wiki/Ebola",
    },
    {
        name: "SARS-CoV-2",
        href: "https://en.wikipedia.org/wiki/COVID-19#SARS-CoV-2_variants",
    },
];

const menu = new Menu(diseases);
console.log(menu.vova);
menu.sara();
menu.init();
