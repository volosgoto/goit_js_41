const diseases = [
    {
        name: "Plague",
        href: "https://en.wikipedia.org/wiki/Plague_(disease)",
    },
    {
        name: "Anthrax",
        href: "https://en.wikipedia.org/wiki/Anthrax",
    },
    {
        name: "Ebola",
        href: "https://en.wikipedia.org/wiki/Ebola",
    },
    {
        name: "SARS-CoV-2",
        href: "https://en.wikipedia.org/wiki/COVID-19#SARS-CoV-2_variants",
    },
];

const rootRef = document.querySelector("#root");
// console.log("rootRef", rootRef);
const menuBtn = document.querySelector("#menu");
// console.log("menuBtn", menuBtn);

function createLi(menuItem) {
    const { name, href } = menuItem;
    let li = document.createElement("li");
    let a = document.createElement("a");

    a.setAttribute("href", href);
    a.setAttribute("target", "_blank");
    a.textContent = name;

    a.classList.add("item__link");
    li.classList.add("item__list");

    li.append(a);

    return li;
}

const menu = diseases.map((menuItem) => {
    return createLi(menuItem);
});
console.log("menu", menu);

const ul = document.createElement("ul");
ul.classList.add("hide");
ul.append(...menu);
console.log(ul);

rootRef.append(ul);

function onBtnMenuClick() {
    ul.classList.toggle("show");

    if (ul.classList.contains("show")) {
        menuBtn.textContent = "close menu";
        return;
    }
    return (menuBtn.textContent = "show menu");
}

// function onBtnMenuClick() {
//   ul.classList.toggle("show");

//   // console.log(ul.classList.contains("pizza"));
//   if (ul.classList.contains("show")) {
//     menuBtn.textContent = "close menu";
//   } else {
//     menuBtn.textContent = "show menu";
//   }
// }

menuBtn.addEventListener("click", onBtnMenuClick);
// diseases.map();
// console.log(
//   createLi({
//     name: "Plague",
//     href: "https://en.wikipedia.org/wiki/Plague_(disease)",
//   })
// );

// let friends = ["alex", "nik", "ygin", "tom"];

// friends.forEach((elem, idx, arr) => {
//   console.log(elem);
// });

// const updatedFriends = friends.map((elem, idx, arr) => {
//   return elem.toUpperCase();
// });
// console.log("updatedFriends", updatedFriends);
