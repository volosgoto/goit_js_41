// const countries = ["USA", "Bratian", "China", "Russia"];
// const vacines = ["Pfizer", "Astrazeneca", "Coronovac", "Sputnik-V"];

// let vacinesBank = function (countriesArr, vacinesArr) {
//   let obj = {};
//   for (let i = 0; i < countriesArr.length; i += 1) {
//     console.log(countriesArr[i]);
//     console.log(vacinesArr[i]);

//     obj[countriesArr[i]] = vacinesArr[i];
//   }
//   return obj;
// };

// console.log(vacinesBank(countries, vacines));

// let admin = {
//   name: "Vova",
//   age: 25,
//   status: "Admin",
//   active: true,
// };

// console.log(admin.name);
// console.log(admin["name"]);

// console.log(admin.status);
// console.log(admin["status"]);

//====

const countries = ["USA", "Bratian", "China", "Russia"];
const vacines = ["Pfizer", "Astrazeneca", "Coronovac", "Sputnik-V"];

let vacinesBank = function (countriesArr, vacinesArr) {
    let obj = {};

    for (const idx in countriesArr) {
        obj[countriesArr[idx]] = vacinesArr[idx];
    }
    return obj;
};

console.log(vacinesBank(countries, vacines));
