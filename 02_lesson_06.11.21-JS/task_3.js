// const myPush = function (arr, ...dataToPush) {
//   let newArr = [];
//   newArr = [...arr, ...dataToPush];
//   return newArr;
// };

const myPush = function (arr, ...dataToPush) {
    return [...arr, ...dataToPush];
};

console.log(myPush([1, 2, 3, 4], "Vova", "sara"));
