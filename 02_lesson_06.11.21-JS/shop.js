let items = [
    { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
    { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
    { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
    { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
];

const shop = {
    items,
    showItems() {
        for (const item of this.items) {
            console.log(
                `id: ${item.id}, name: ${item.name}, price: ${item.price}, qty: ${item.qty}`
            );
        }
    },
    addItem(newItem) {
        this.items.push(newItem);
    },

    updateItem(itemName, nameToUpdate) {
        for (const item of this.items) {
            if (itemName === item.name) {
                item.name = nameToUpdate;
            }
            console.log(`we are not find ${itemName}`);
        }
    },
    removeItem(itemToDelete) {
        for (const item of this.items) {
            if (itemToDelete === item.name) {
                let idx = this.items.indexOf(item);
                console.log(idx);
                this.items.splice(idx, 1);
            }
        }
    },
};

const newItem = {
    id: "12",
    name: "Kiwi",
    price: 67,
    qty: 600,
    category: "fruits",
};

// shop.addItem(newItem);

// shop.updateItem("pizza", "potato +");

// console.log(shop.updateItem("pizza", "potato +"));

shop.removeItem("potato");

shop.showItems();

// C - CREATE
// R - READ
// U - UPDATE
// D - DELETE
