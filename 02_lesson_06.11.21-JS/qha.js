// let a = 10;
// let b = a + 1;

// console.log("a", a);
// console.log("b", b);
// console.log(a === b);
// ============================

// let a = [1, 2, 3, 4];
// let b = a;

// b.push(5);
// a.push("Vova");

// console.log("a", a);
// console.log("b", b);
// console.log(a === b);
// ============================

// let a = [1, 2, 3, 4];
// let b = a;

// let c = [1, 2, 3, 4];

// console.log(a === b);
// console.log(c === a);
// console.log(c === b);

//====================================
// Array copy

// Concat
// let arr = [1, 2, 3, 4, 5];
// // let fruit = ["apple", "banannes", "melon"];
// // let cities = ["Kyiv", "Lviv", "Odessa"];

// let newArr = arr.concat();
// console.log(newArr);

// console.log(newArr === arr);

// Slice
// let arr = [1, 2, 3, 4, 5];
// let newArr = arr.slice();

// console.log(newArr);
// console.log(newArr === arr);

// Array copy
// Spred
// let arr = [1, 2, 3, 4, 5];
// let fruit = ["apple", "banannes", "melon"];
// let newArr = ["Sara", ...fruit, ...arr, "Vova"];

// console.log(newArr);
// console.log(newArr === arr);

// let admin = {
//     name: "Vova",
//     age: 25,
//     status: "Admin",
//     active: true,
// };

// let user = {
//     ...admin,
//     status: "User",
//     active: false,
//     cart: false,
//     friends: [],
// };

// console.log(user);
// ============================

// let arr = [1, 2, 3, 4, 5];
// arr.push(6, "Vova", "Sara", 100);
// console.log(arr);

let admin = {
    name: "Vova",
    age: 25,
    status: "Admin",
    active: true,
};

// console.log(admin.name);
// console.log(admin["name"]);

// console.log(admin.status);
// console.log(admin["status"]);

console.log(admin["name"]);

admin["name"] = "Sara";
admin[countriesArr[i]] = vacArr[i];
console.log(admin);
