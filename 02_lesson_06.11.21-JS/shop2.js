let items = [
    { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
    { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
    { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
    { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
];

const shop = {
    items,
    showItems() {
        for (const item of this.items) {
            console.log(
                `id: ${item.id}, name: ${item.name}, price: ${item.price}, qty: ${item.qty}`
            );
        }
    },
    // addItem(newItem) {
    //   this.items.push(newItem);
    // },

    addItem(id, name, price, qty, category) {
        let itemObj = { id, name, price, qty, category };
        // this.items.push(itemObj);
        this.items = [...this.items, itemObj];
    },

    updateItem(itemName, nameToUpdate) {
        for (const item of this.items) {
            if (itemName === item.name) {
                item.name = nameToUpdate;
            }
            console.log(`we are not find ${itemName}`);
        }
    },
    removeItem(itemToDelete) {
        // console.log(this.items);
        for (const item of this.items) {
            if (itemToDelete === item.name) {
                let idx = this.items.indexOf(item);
                console.log(idx);
                this.items.splice(idx, 1);
            }
        }
    },
};

shop.addItem("12", "Kiwi", 67, 600, "fruits");

// shop.updateItem("pizza", "potato +");

// console.log(shop.updateItem("pizza", "potato +"));

shop.removeItem("potato");

shop.showItems();

// C - CREATE
// R - READ
// U - UPDATE
// D - DELETE
