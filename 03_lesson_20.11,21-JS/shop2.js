let items = [
    { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
    { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
    { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
    { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
];

const shop = {
    name: "Mango",
    adress: "Kyiv",
    items,

    showItems() {
        for (const item of this.items) {
            // console.log(`${item.id}, ${item.name}, ${item.price}`);
            const { id, name, price, qty, color } = item;
            console.log(`${id}, ${name}, ${price}, ${qty}, ${color}`);
        }
        // console.log(this.items);
    },
    addItem(newItem) {
        // this.items.push(newItem);
        const item = { ...newItem, id: this.generetId(), color: "red" };
        // item.color = "red";
        // console.log(item);
        this.items = [...this.items, item];
    },
    updateItem(oldName, newName) {
        for (const item of this.items) {
            if (item.name === oldName) {
                item.name = newName;
                // console.log(item.name);
            }
        }
    },
    deliteItem(delitItemName) {
        for (const item of this.items) {
            if (item.name === delitItemName) {
                let idx = this.items.indexOf(item);
                this.items.splice(idx, 1);
            }
        }
    },
    generetId() {
        let rend = Math.random();
        let rendString = String(rend).slice(2);
        // console.log(rendString);
        return rendString;
    },
};
// shop.showItems();
shop.addItem({
    name: "chery",
    price: 20,
    qty: 380,
    category: "fruits",
});

shop.updateItem("bananes", "bananes+");

shop.deliteItem("tomatoes");
// console.log(shop.name);
shop.showItems();

// function generetId() {
//   let rend = Math.random();
//   let rendString = String(rend).slice(2);
//   // console.log(rendString);
//   return rendString;
// }
// generetId();

// const arr = [1, 2, 3, 5, 6];
// // arr.push(100, 200, 300, "Vova");
// // console.log(arr);

// function myPush(arr, ...element) {
//   arr = [...arr, ...element];
//   return arr;
// }
// const result = myPush(arr, 56, 64, 58, 2);
// console.log(result);
