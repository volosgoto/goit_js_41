class StringBuilder {
    constructor(baseValue) {
        this.value = baseValue;
    }

    getValue = function () {
        return this.value;
    };

    padEnd = function (str) {
        this.value += str;
    };

    padStart = function (str) {
        this.value = str + this.value;
    };

    padBoth = function (str) {
        this.padStart(str);
        this.padEnd(str);
    };
}

const builder = new StringBuilder(".");
//   console.log(builder.getValue()); // '.'
//   builder.padStart('^');
//   console.log(builder.getValue()); // '^.'
//   builder.padEnd('^');
//   console.log(builder.getValue()); // '^.^'
//   builder.padBoth('=');
//   console.log(builder.getValue()); // '=^.^='

builder.padStart("^").padEnd("^").padBoth("=");
