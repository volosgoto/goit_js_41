let items = [
    { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
    { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
    { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
    { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
];

const shop = {
    name: "Mango",
    adress: "Kyiv",
    items,

    showItems() {
        for (const item of this.items) {
            // console.log(`${item.id}, ${item.name}, ${item.price}`);
            const { id, name, price, qty } = item;
            console.log(`${id}, ${name}, ${price}, ${qty}`);
        }
        // console.log(this.items);
    },
    addItem(newItem) {
        this.items.push(newItem);
    },
    updateItem(oldName, newName) {
        for (const item of this.items) {
            if (item.name === oldName) {
                item.name = newName;
                // console.log(item.name);
            }
        }
    },
    deliteItem(delitItemName) {
        for (const item of this.items) {
            if (item.name === delitItemName) {
                let idx = this.items.indexOf(item);
                this.items.splice(idx, 1);
            }
        }
    },
};
// shop.showItems();
shop.addItem({
    id: "id-5",
    name: "chery",
    price: 20,
    qty: 380,
    category: "fruits",
});

shop.updateItem("bananes", "bananes+");

shop.deliteItem("tomatoes");
// console.log(shop.name);
shop.showItems();
