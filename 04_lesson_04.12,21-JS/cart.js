// let items = [
//   { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
//   { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
//   { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
//   { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
// ];

// const cart = [
//     { name: "apples", price: 55, qty: 3, category: "fruits" },
//     { name: "potato", price: 23, qty: 5, category: "fruits" },
// ];

// function getCartSum(cart) {
//     let totalCartSum = cart.reduce((total, cartItem) => {
//         return total + cartItem.price * cartItem.qty;
//     }, 0);
//     return totalCartSum;
// }

// console.log(getCartSum(cart));
// // const salarys = [8000, 12000, 16000, 18000, 24000];

// // let totalSalarys = salarys.reduce((total, elem) => {
// //   return total + elem;
// // }, 0);

// // console.log(totalSalarys);

// ==================================
// CART SIMPLE
// const cart = [
//   { name: "apples", price: 55, qty: 3, category: "fruits" },
//   { name: "potato", price: 23, qty: 5, category: "fruits" },
// ];

// function getCartSum(cart) {
//   let totalCartSum = cart.reduce((total, cartItem) => {
//     return total + cartItem.price * cartItem.qty;
//   }, 0);
//   return totalCartSum;
// }

// console.log(getCartSum(cart));

// ==========================================
// CART CLASS

let products = [
    { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
    { id: "id-2", name: "potato", price: 23, qty: 75, category: "fruits" },
    { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
    { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
];

class Cart {
    constructor(products) {
        this.products = products;
        this.items = [];
    }

    showProducts() {
        console.log(this.products);
    }

    showCart() {
        console.log(this.items);
    }

    addToCart(newItem, quantity) {
        const product = this.products.find((product) => {
            return product.name === newItem && product.qty >= quantity;
        });
        // console.log(product);
        if (!product) {
            console.log("не можу додати такий товар у корзину");
            return;
        }
        const productToAdd = { ...product, qty: quantity };
        this.items.push(productToAdd);

        // this.products = this.updateProducts(newItem, quantity);
        this.updateProducts(newItem, quantity);
    }

    updateCart(productName, quantity) {
        const updatedItems = this.items.map((item) => {
            if (item.name === productName) {
                return {
                    ...item,
                    qty: item.qty - quantity,
                };
            }
            return item;
        });
        this.items = updatedItems;

        const updatedProducts = this.products.map((product) => {
            if (product.name === productName) {
                return {
                    ...product,
                    qty: product.qty + quantity,
                };
            }
            return product;
        });
        this.products = updatedProducts;
    }

    removeFromCart(productName) {
        let qtyGlobal = 0;
        let nameGlobal = "";
        const removedItem = this.items.filter((item) => {
            if (item.name === productName) {
                nameGlobal = item.name;
                qtyGlobal = item.qty;
            }
            return item.name !== productName;
        });

        this.items = removedItem;

        if ((nameGlobal = productName)) {
            const updatedProducts = this.products.map((product) => {
                if (product.name === productName) {
                    return {
                        ...product,
                        qty: product.qty + qtyGlobal,
                    };
                }
                return product;
            });
            this.products = updatedProducts;
        }
    }

    updateProducts(productName, quantity) {
        const updatedProducts = this.products.map((product) => {
            if (product.name === productName) {
                return {
                    ...product,
                    qty: product.qty - quantity,
                };
            }
            return product;
        });
        this.products = updatedProducts;
    }
}
const cart = new Cart(products);

cart.addToCart("potato", 50);
cart.addToCart("apples", 5);
cart.addToCart("bananes", 10);
// cart.updateCart("potato", 5);
cart.removeFromCart("potato");
// console.log(cart.updateProducts("potato", 50));
cart.showCart();
cart.showProducts();
