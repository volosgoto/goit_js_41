class Shop {
    #items;

    constructor(items, name, adress) {
        this.#items = items;
        this.name = name;
        this.adress = adress;
    }

    showItems() {
        for (const item of this.#items) {
            // console.log(`${item.id}, ${item.name}, ${item.price}`);
            const { id, name, price, qty } = item;
            console.log(`${id}, ${name}, ${price}, ${qty}`);
        }
        // console.log(this.items);
    }

    addItem({ name, price, qty, category }) {
        const newItem = {
            // id: this.generetID(),
            name,
            price,
            qty,
            category,
        };
        // this.items = [...this.items, newItem];
        this.#items = [...this.#items, { ...newItem, id: this.#generetID() }];
    }

    // addItem(newItem) {
    //   this.items.push(newItem);
    // }

    updateItem(oldName, newName) {
        for (const item of this.#items) {
            if (item.name === oldName) {
                item.name = newName;
                // console.log(item.name);
            }
        }
    }

    deliteItem(delitItemName) {
        for (const item of this.#items) {
            if (item.name === delitItemName) {
                let idx = this.#items.indexOf(item);
                this.#items.splice(idx, 1);
            }
        }
    }

    #generetID(params) {
        return Math.random().toString().slice(2);
    }
}

let items = [
    { id: "id-1", name: "apples", price: 55, qty: 500, category: "fruits" },
    { id: "id-2", name: "potato", price: 23, qty: 875, category: "fruits" },
    { id: "id-3", name: "bananes", price: 50, qty: 400, category: "fruits" },
    { id: "id-4", name: "tomatoes", price: 35, qty: 650, category: "fruits" },
];

const shop = new Shop(items, "Silpo", "Kyiv");

shop.addItem({
    name: "chery",
    price: 20,
    qty: 380,
    category: "fruits",
});

shop.updateItem("bananes", "bananes+");

shop.deliteItem("tomatoes");

shop.showItems();

let vova = {
    name: "Vova",
    age: 25,
};

let sara = {
    ...vova,
    name: "Sara",
    id: "156asdasfsad",
};
